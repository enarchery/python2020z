from turtle import *

reset()

shape("turtle")

# Baumkrone
pensize(5)
pencolor("black")
fillcolor("green")
begin_fill()
circle (100)
end_fill()

# Stamm
pencolor("black")
fillcolor("brown")
begin_fill()
forward(20)
right(90)
forward(300)
right(90)
forward(40)
right(90)
forward(300)
right(90)
forward(20)
end_fill()
hideturtle()