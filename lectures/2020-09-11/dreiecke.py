from turtle import *

reset()

def dreieck():
    fd(100)
    lt(120)
    fd(100)
    lt(120)
    fd(100)
    lt(120)

pencolor("red")
fillcolor("yellow")
begin_fill()

dreieck()
fd(200)

dreieck()
fd(200)

dreieck()
fd(200)

end_fill()
