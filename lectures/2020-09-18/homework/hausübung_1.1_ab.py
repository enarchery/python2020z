#Hausübung ad-hoc Übung 1.1 a, b
#1.1 a) Stellen Sie mit der print-Anweisung einen Datensatz aus folgenden einzelnen Bestandteilen zusammen: Name, Vorname, Strasse, Hausnr., PLZ, Wohnort

print("Name,","Vorname,","Strasse,","Hausnr.,","PLZ,","Wohnort")

#1.1 b) Schreiben Sie einen Befehl, der alle Zahlen von 1 bis 5 addiert, subtrahiert, multipliziert und dividiert

print((5*(5+1))/2)
print(1-2-3-4-5)
print(1*2*3*4*5)
print(((((1/2)/3)/4)/5))
print(1/2/3/4/5)