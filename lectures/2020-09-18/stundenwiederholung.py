#
# Stundenwiederholung vom 18. September 2020
#

# name = input("Bitte geben Sie Ihren Namen ein...")
# print(("Guten Morgen, " + name + "! ") * 10)

s = "Donaudampfschiffsfahrtgesellschaft"
print(s[0:5])
print(s[:5])   # erster Index weggelassen => starte vom Anfang
print(s[22:34])
print(s[22:])  # letzten Index weggelassen => gehe bis zum Ende5
print(s[-12:])

#
name = "Ana"
alter = 22
print(name + " ist", alter, "alt.")
print(name + " ist " + str(alter) + " alt.")  # str(x) macht aus einer Variable einen String


