
def durchschnitt(name, note1, note2, note3, note4, note5, note6) :
  note1 = float(note1)  
  note2 = float(note2) 
  note3 = float(note3)  
  note4 = float(note4)  
  note5 = float(note5)  
  note6 = float(note6)  
  sum = note1 + note2 + note3 + note4 + note5 + note6 #Summe hier definieren
  schnitt = float(sum / 6)
  
  return name + " Notendurschnitt: " + str(round(schnitt, 2)) #definiert Ausgabe

print(durchschnitt("Max Muster",3,5,4,4,3,3))
print(durchschnitt("Heidi Huber",5,5,6,5,4.5,4))
