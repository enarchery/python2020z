#1.2 c und e direkt nacheinander

from turtle import *

reset()

pensize(5)
pencolor("blue")
left(45)
forward(100)

pencolor("red")
right(90)
forward(100)

pencolor("cyan")
left(90)
forward(100)

pencolor("black")
right(90)
forward(100)

clear()
penup()
home()
pendown()

pencolor("red")
forward(100)
left(120)
forward(100)
left(120)
forward(100)

pencolor("blue")
right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

pencolor("lime")
right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)