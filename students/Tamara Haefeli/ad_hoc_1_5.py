#1.5

#a)

from turtle import *

reset()

size = numinput("Eingabefenster", "Bitte Seitenlänge eingeben:")

#grünes Dreieck
left(30)
pencolor("red")
pensize(5)
fillcolor("lime")
begin_fill()
forward(size)
left(120)
forward(size)
left(120)
forward(size)
end_fill()

#blaues Dreieck
fillcolor("cyan")
begin_fill()
forward(size*2)
left(120)
forward(size*2)
left(120)
forward(size*2)
end_fill()

#gelbes Dreieck
fillcolor("yellow")
begin_fill()
forward(size*3)
left(120)
forward(size*3)
left(120)
forward(size*3)
end_fill()