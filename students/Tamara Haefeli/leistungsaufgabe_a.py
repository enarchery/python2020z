#Autor: Tamara Haefeli
#Datum: 17.09.2020
#leistungsaufgabe_a.py

from turtle import *

setup(400,400)
bgcolor("red")     #Hintergrundfarbe rot
reset()
speed(10)          #schnell(st)e Zeichengeschwindigkeit
ht()               #Pfeil verstecken (hideturtle)
pu()               #Stift anheben (penup)


fd(125)            #geradeaus die Anfangsstelle anvisieren (forward)    

# Weisses Kreuz
pd()               #Stift ablegen (pendown)
pencolor("white")  #Stiftfarbe weiss
fillcolor("white") #Füllfarbe weiss
begin_fill()       #Beginn des Füllens
lt(90)             #links um 90 Grad (left)   
fd(37.5)         
lt(90)
fd(87.5)
rt(90)             #rechts um 90 Grad (right)
fd(87.5)
lt(90)
fd(75)
lt(90)
fd(87.5)
rt(90)
fd(87.5)
lt(90)
fd(75)
lt(90)
fd(87.5)
rt(90)
fd(87.5)
lt(90)
fd(75)
lt(90)
fd(87.5)
rt(90)
fd(87.5)
lt(90)
fd(37.5)
end_fill()        #weisse Füllung ausführen/beenden

exitonclick()     #schliesst das Fenster bei Klick auf die Grafik
