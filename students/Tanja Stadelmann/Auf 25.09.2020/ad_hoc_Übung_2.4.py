#ad hoc Übung 2.4

def berechne_notendurchschnitt(noteeins, notezwei, notedrei, notevier, notefünf, notesechs):
    notendurchschnitt = (noteeins + notezwei + notedrei + notevier + notefünf + notesechs) / 6
    return notendurchschnitt

notendurchschnitt = berechne_notendurchschnitt (4, 5.5, 6, 4, 5, 4.5)
notendurchschnitt = round(notendurchschnitt, 2)
print("Max Mustermann", "Notendurchschnitt:", notendurchschnitt)