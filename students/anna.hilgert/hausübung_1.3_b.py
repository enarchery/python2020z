#Hausübung ad-hoc Übung 1.3 b
#1.3 b) Fünf Quadrate mit einem roten Rahmen sind überlappend im Kreis angeordnet, zu unterst beginnend mit der Füllung Cyan, Geld, Pink, Blau, Grün

from turtle import *

reset()

pensize(5)
pencolor("red")
def quadrat():
    fd(100)
    lt(90)
    fd(100)
    lt(90)
    fd(100)
    lt(90)
    fd(100)
    
fillcolor("cyan")
begin_fill()
quadrat()
end_fill()
lt(20)

fillcolor("yellow")
begin_fill()
quadrat()
end_fill()
lt(20)

fillcolor("pink")
begin_fill()
quadrat()
end_fill()
lt(20)

fillcolor("blue")
begin_fill()
quadrat()
end_fill()
lt(20)

fillcolor("green")
begin_fill()
quadrat()
end_fill()