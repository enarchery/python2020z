#Hausübung ad-hoc Übung 2.4

#2.4 Schreiben Sie eine Funktion, die als Eingabeparameter einen Namen sowie 6 Noten erhält. Die Rückgabe soll einen String mit folgendem Aufbau erzeugen "Name, Notendurchschnitt: Notenschnitt"
#Der Schnitt sollte dabei auf 2 Dezimalstellen gerundet werden.

def notendurchschnitt(name, note1, note2, note3, note4, note5, note6):
    notenschnitt=round(((note1+note2+note3+note4+note5+note6)/6),2)
    return name + " Notendurchschnitt: " + str(float(notenschnitt))

print(notendurchschnitt("Max Mustermann,",4,5.5,6,4,5,4.5))


