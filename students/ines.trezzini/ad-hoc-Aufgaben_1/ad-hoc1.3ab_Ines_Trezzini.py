# ad-hoc 1.3 a), S. 20

from turtle import *

# a) buntes Windrad
reset()
left(20)
pencolor("red")
pensize(5)
fillcolor("lime")
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

fillcolor("cyan")
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

fillcolor("yellow")
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

# weiter zur nächsten Zeichnung
penup()
forward(300)
right(20)
pendown()

# b) fünf Quadrate (beim "untersten" Quadrat" anfangen, damit alles richtig überlappt)
# Stiftfarbe und -dicke
pencolor("red")
pensize(5)

# a) hellblaues Quadrat

right(20)
fillcolor("cyan")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

# a) gelbes Quadrat

left(5)
fillcolor("yellow")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

# a) violettes Quadrat
left(5)
fillcolor("magenta")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

# a) blaues Quadrat
left(5)
fillcolor("blue")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

# a) grünes Quadrat

# left(200)
left(20)
forward(100)
right (270)
fillcolor("lime")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()