#  ad-hoc 2.4

def notendurchschnitt(name, note1,note2,note3,note4,note5,note6):
    note1 = float(note1)
    note2 = float(note2)
    note3 = float(note3)
    note4 = float(note4)
    note5 = float(note5)
    note6 = float(note6)
    durchschnitt=((note1+note2+note3+note4+note5+note6)/6)
    durchschnitt = round(durchschnitt, 2)
    return name+", Notendurchschnitt: "+str(durchschnitt)

print(notendurchschnitt("Max Mustermann",4,5.5,6,4,5,4.5))

# Ursprüngliche Versuche gaben Ergebnis jeweils in einfachen Anführungszeichen aus, warum?
# Beispiel: return name+", Notendurchschnitt:",durchschnitt bzw. return name,", Notendurchschnitt:",durchschnitt