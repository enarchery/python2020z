# ad-hoc 2.6

from turtle import *

# Programm für drei Vierecke
def vier_vierecke():
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    
    penup()
    right(90)
    forward(60)
    pendown()
  
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    
    penup()
    right(90)
    forward(60)
    pendown()
    
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    
    penup()
    right(90)
    forward(60)
    pendown()
    
vier_vierecke()

# fragen, wie viele Vierecke gezeichnet werden sollen
viereckmenge = numinput("Eingabefenster", "Bitte Anzahl Vierecke eingeben:")
zahl=int(viereckmenge)
for viereckmenge in range(zahl):
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    
    penup()
    right(90)
    forward(60)
    pendown()

