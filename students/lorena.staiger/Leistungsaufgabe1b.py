from turtle import *

# Definieren des Eingabefensters und der Zahlenrundung
size=numinput("Eingabefenster", "Bitte Länge des Schweizerkreuzes eingeben")
site=int(size)

# Hintergrund
hideturtle()

pencolor("red")
fillcolor("red")

penup()
fd(size/2)
lt(90)
pendown()

begin_fill()
fd(size/2)
lt(90)
fd(size)
lt(90)
fd(size)
lt(90)
fd(size)
lt(90)
fd(size/2)
end_fill()

# Schweizerkreuz
pencolor("white")
fillcolor("white")

penup()
lt(90)
fd(size/4.2)
lt(90)
pendown()

begin_fill()
fd(size/14)
rt(90)
fd(size/5)
lt(90)
fd(size/5)
rt(90)
fd(size/7)
rt(90)
fd(size/5)
lt(90)
fd(size/5)
rt(90)
fd(size/7)
rt(90)
fd(size/5)
lt(90)
fd(size/5)
rt(90)
fd(size/7)
rt(90)
fd(size/5)
lt(90)
fd(size/5)
rt(90)
fd(size/14)
end_fill()

