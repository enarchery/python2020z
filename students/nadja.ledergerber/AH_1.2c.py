from turtle import *

reset()

shape("turtle")

pensize(5)
pencolor("blue")

left(45)
forward(100)

pencolor("red")

right(90)
forward(100)

pencolor("cyan")

left(90)
forward(100)

pencolor("black")

right(90)
forward(100)