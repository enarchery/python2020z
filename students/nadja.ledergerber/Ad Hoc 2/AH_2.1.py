from turtle import *

reset()

shape("turtle")

pencolor("red")
pensize(5)

def dreieck(seitenlaenge,fuellfarbe):
    fillcolor(fuellfarbe)
    begin_fill()
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    end_fill()
    
    
left(30)

dreieck(100,"green")
dreieck(100,"cyan")
dreieck(100,"yellow")
