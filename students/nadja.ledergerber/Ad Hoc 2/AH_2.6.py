from turtle import *

reset()

speed(10)
shape("turtle")

pencolor("red")
pensize(5)


    
def jump (strecke):
    penup()
    forward(strecke)
    pendown()
    
def viereck(seitenlaenge,fuellfarbe):
    fillcolor(fuellfarbe)
    begin_fill()
    forward(seitenlaenge)
    left(90)
    forward(seitenlaenge)
    left(90)
    forward(seitenlaenge)
    left(90)
    forward(seitenlaenge)
    left(90)
    end_fill()
    jump(110)

i = 1
x = numinput("Anzahl","Anzahl Vierecke eingeben")
    
while i <= x:
    viereck(100,"green")
    i=i+1


# def dreieck3():
#     dreieck(100,"green")
#     dreieck(100,"cyan")
#     dreieck(100,"yellow")
#     jump(30,100)
#     
# def jump (winkel,strecke):
#     left(winkel)
#     penup()
#     forward(strecke)
#     pendown()
#     
# i=1
# 
# while i <= 12:
#     dreieck3()
#     i=i+1
#     
# hideturtle()