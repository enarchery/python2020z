from turtle import *

reset()

size = numinput("Schweizerkreuz", "Bitte Seitenlänge angeben")

hideturtle()
speed(10) #standard speed is too slow!

#Hintergrund
pencolor("red")
pensize(1)

fillcolor("red")
begin_fill()
left(180)
forward (size/2)
left(90)
forward (size)
left(90)
forward (size)
left(90)
forward (size)
left(90)
forward (size/2)
end_fill()

penup() #hebe Stift an
left(90)
forward(size/5)
pendown() #setze Stift wieder ab

#Kreuz

a = size/5 #Ich muss die Seitengrösse auch "neutral" machen und es soll sich an size orientieren

pencolor("white")
fillcolor("white")
begin_fill()

right(90)
forward(a/2)
left(90)
forward(a)
right(90)
forward(a)
left(90)
forward(a)
left(90)
forward(a)

right(90)
forward(a)
left(90)
forward(a)
left(90)
forward(a)

right(90)
forward(a)
left(90)
forward(a)
left(90)
forward(a)

right(90)
forward(a)
left(90)
forward(a/2)

end_fill()

