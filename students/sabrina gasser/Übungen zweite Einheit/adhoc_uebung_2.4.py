#2.4 adhoc Notendurchschnitt
#Sabrina Gasser
#22.09.2020

def notendurchschnitt(name, zahl1, zahl2, zahl3, zahl4, zahl5, zahl6):
    zahl1 = float(zahl1)
    zahl2 = float(zahl2)
    zahl3 = float(zahl3)
    zahl4 = float(zahl4)
    zahl5 = float(zahl5)
    zahl6 = float(zahl6)
    #float und nicht int weil ich nicht ganze Zahlen brauche
    notenschnitt = round(((zahl1 + zahl2 + zahl3 + zahl4 + zahl5 + zahl6)/6), 2)
    #round um Ergebnis zu runden. Die 2 vor der Schlussklammer zeigt an auf wie viele Nachkommastellen. Gemäss Auftrag: 2 Stellen.  

    return name + ", Notendurchschnitt: "+ str(notenschnitt)
    #notenschnitt mit str davor versetzen, da sonst immer Fehlermeldung kam. Text und Zahl nicht addierbar

print(notendurchschnitt("Max Mustermann", 4, 5.5, 6, 4, 5, 4.5))

