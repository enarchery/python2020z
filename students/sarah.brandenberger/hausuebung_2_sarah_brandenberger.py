#author sarah brandenberger
#date 23.09.2020
#hausuebung 2

from turtle import *
from math import *

#Ad hoc Übungen
# ad hoc 2.1 a.)
left(150)

def dreieck_zeichnen(fuellfarbe): 
    pensize(5)
    pencolor("red")
    fillcolor(fuellfarbe)
    begin_fill()
    left(120)
    forward(100)
    left(120)
    forward(100)
    left(120)
    forward(100)
    left(120)
    end_fill()

dreieck_zeichnen("turquoise")
dreieck_zeichnen("lime")
dreieck_zeichnen("yellow")


# ad hoc 2.4
# def notendurchschnitt():
#     name = textinput("Name", "Geben Sie den Namen ein")
#     note1 = numinput("Noteneingabe", "Geben Sie die 1. Note ein")
#     note2 = numinput("Noteneingabe", "Geben Sie die 2. Note ein")
#     note3 = numinput("Noteneingabe", "Geben Sie die 3. Note ein")
#     note4 = numinput("Noteneingabe", "Geben Sie die 4. Note ein")
#     note5 = numinput("Noteneingabe", "Geben Sie die 5. Note ein")
#     note6 = numinput("Noteneingabe", "Geben Sie die 6. Note ein")
#     schnitt = (note1 + note2 + note3 + note4 + note5 + note6)/6
#     print(name, "Notenschnitt:", round(schnitt, 2))
# notendurchschnitt()

def notendurchschnitt(name, note1, note2, note3, note4, note5, note6):
    schnitt = (note1 + note2 + note3 + note4 + note5 + note6)/6
    print(name, "Notenschnitt:", round(schnitt, 2))
notendurchschnitt("Heiri", 4.25, 4.75555, 3.555, 5.5588, 5.665555, 6.5555)

# ad hoc 2.6
penup()
forward(300)
pendown()

def viereck_zeichnen():
        pensize(5)
        pencolor("red")     
        forward(100)
        left(90)
        forward(100)
        left(90)
        forward(100)
        left(90)
        forward(100)
        left(120)

counter = 0
anzahl = numinput("Anzahl", "Geben Sie die Anzahl Dreiecke ein")
while counter < anzahl:
    viereck_zeichnen()
    counter = counter + 1
       
#Übungsblatt
#Aufgabe 2.1
def guten_morgen():
    print("Guten Morgen!") 
guten_morgen()

#Aufgabe 2.2
def guten_morgen(name): 
    print("Guten Morgen", name + "!")
guten_morgen("Ana")

#Aufgabe 2.3 a.)
def guten_morgen(): 
    gruss = textinput("Gruss", "Geben Sie hier Ihren Namen ein")
    print("Guten Morgen", gruss + "!")
guten_morgen()

#Aufgabe 2.3 b.)
def flaeche_viereck (laenge, breite):
    flaeche = laenge*breite
    return flaeche
print("Die Fläche beträgt: ", flaeche_viereck(10,20), "m2")


#Aufgabe 2.8 a.)
zahl = 0
while zahl <= 9:
    zahl = zahl + 1
    print(zahl)

#Aufgabe 2.8 c.)
for i in range(10,0,-3):
    print(i)



