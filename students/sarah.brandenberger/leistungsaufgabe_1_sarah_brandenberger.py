#author sarah brandenberger
#date 23.09.2020
#leistungsaufgabe 1

from turtle import *

laenge = numinput("Seitenlänge", "Geben Sie hier die Seitenlänge der Flagge ein:")

fillcolor("red")
begin_fill()
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
left(90) 
forward(laenge)
left(90)
end_fill()

penup()
forward(laenge/8)
right(90)
backward(laenge/2)
pendown()

fillcolor("white")
begin_fill()
forward(laenge /8)
left(90)
forward(laenge/4)
right(90)
forward(laenge/4)
left(90)
forward(laenge/4)
left(90)
forward(laenge/4)
right(90)
forward(laenge/4)
left(90)
forward(laenge/4)
left(90)
forward(laenge/4)
right(90)
forward(laenge/4)
left(90)
forward(laenge/4)
left(90)
forward(laenge/4)
right(90)
forward(laenge/4)
left(90)
forward(laenge /8)
end_fill()


hideturtle()
