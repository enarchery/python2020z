#Autor:Sarina Stutz
#Datum:13.09.2020
#ad hoc Übungen 1.2 c) e)
#Übung 1.2 c)
from turtle import *
reset ()
pencolor("blue")
pensize (5)
right (315)
forward (100)
pencolor("red")
right (90)
forward (100)
pencolor("cyan")
left (90)
forward (100)
pencolor("black")
right (90)
forward (100)
reset ()
#Übung 1.2e)
pencolor("blue")
pensize (5)
right (180)
forward (100)
right (120)
forward (100)
right (120)
forward (100)
pencolor("red")
left (60)
forward (100)
left (120)
forward (100)
left (120)
forward (100)
pencolor("lightgreen")
forward (100)
left (120)
forward (100)
left (120)
forward (100)
reset ()