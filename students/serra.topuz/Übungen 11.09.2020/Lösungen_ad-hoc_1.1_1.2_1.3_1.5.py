# Übung 1.1 a)
print("Topuz, Serra, Köschenrütistrasse, 57, 8052, Zürich")
#oder
print("Nachname,","Vorname,","Strasse,","Postleitzahl,","Wohnort")

# Übung 1.1 b)
print(1+2+3+4+5)
print(5-3-4-2-1)
print(1*2*3*4*5)
print(5/4/3/2/1)

#Übung 1.2 c)
from turtle import*
reset()

pensize(5)
pencolor("blue")
lt(45);fd(100)
pencolor("red")
rt(90);fd(100)
pencolor("cyan")
lt(90);fd(100)
pencolor("black")
rt(90);fd(100)

#Platz machen
penup()
rt(100);fd(300);
pendown()

#Übung 1.2 e)
pencolor("blue")
rt(35);fd(100);rt(120);fd(100);rt(120);fd(100)
pencolor("lime")
fd(100);rt(120);fd(100);rt(120);fd(100)
pencolor("red")
fd(100);rt(120);fd(100);rt(120);fd(100)

#Platz machen
penup()
fd(200);rt(90);fd(400);rt(180);lt(90)
pendown()

#Übung 1.3 b)
fillcolor("cyan")
begin_fill()
lt(40);fd(100);rt(90);fd(100);rt(90);fd(100);rt(90);fd(100)
end_fill()

fillcolor("yellow")
begin_fill()
rt(170);fd(100);rt(90);fd(100);rt(90);fd(100);rt(90);fd(100)
end_fill()

fillcolor("magenta")
begin_fill()
rt(170);fd(100);rt(90);fd(100);rt(90);fd(100);rt(90);fd(100)
end_fill()

fillcolor("blue")
begin_fill()
rt(170);fd(100);rt(90);fd(100);rt(90);fd(100);rt(90);fd(100)
end_fill()

fillcolor("lime")
begin_fill()
rt(150);fd(100);rt(90);fd(100);rt(90);fd(100);rt(90);fd(100)
end_fill()

#Platz machen
penup()
rt(40);fd(300);lt(110);fd(600);lt(100)
pendown()

#Übung 1.5 a)
#size: 200
size = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")

fillcolor("yellow")
begin_fill()
lt(30);fd(size);lt(120);fd(size);lt(120);fd(size)
end_fill()

penup()
lt(120);fd(size)
pendown()

#size: 50
size = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")

fillcolor("lime")
begin_fill()
lt(15);fd(size);lt(120);fd(size);lt(120);fd(size)
end_fill()

#size: 100
size = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")

fillcolor("cyan")
begin_fill()
lt(40);fd(size);rt(120);fd(size);rt(120);fd(size)
end_fill()