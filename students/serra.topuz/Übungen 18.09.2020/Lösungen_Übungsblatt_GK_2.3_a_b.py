#Übung 2.3 a) Guten Morgen Funktionen mit Rückgabewert und Parameter

def guten_morgen(name):
    gruss = "Guten Morgen "+name+"!"
    return gruss

gruss = guten_morgen("Ana")

print(gruss)


#Übung 2.3 b) Fläche Rechteck

def flaeche_rechteck(laenge, breite):
    flaeche = laenge*breite
    return flaeche

flaeche = flaeche_rechteck(10, 20)

print("Die Fläche beträgt", flaeche, "m2.")
    
